package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.text.ParseException;

public class PeselTests {

    @Test()
    public void TestPesel(){
        Pesel pesel;
        try{
            pesel =  PeselParser.Parse("44051401359");
            int peselDigit = pesel.getDigit(-1);
            Assert.assertEquals(-1, peselDigit);
        }
        catch (ParseException e)
        {
            Assert.fail();
            return;
        };
    }

}
