package agh.tests;

import agh.qa.Pesel;
import agh.qa.PeselExtractor;
import agh.qa.PeselParser;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.text.ParseException;
import java.time.LocalDate;


public class PeselExtractorTests {
    @Test()
    public void TestDate(){
        Pesel pesel;
        try{
            pesel =  PeselParser.Parse("44051401359");
        }
        catch (ParseException e)
        {
            Assert.fail();
            return;
        }
        PeselExtractor extractor = new PeselExtractor( pesel);
        LocalDate date = extractor.GetBirthDate();
        Assert.assertEquals("1944-05-14", date.toString());
    }

    @DataProvider
    public Object[][] peselSexTestDataProvider() {
        return new Object[][]{
                {"44051401359", "Male"},
                {"01240521467", "Female"}
        };
    }

    @Test(dataProvider = "peselSexTestDataProvider")
    public void TestSex(String peselStr, String shouldBeSex){
        Pesel pesel;
        try{
            pesel =  PeselParser.Parse(peselStr);
        }
        catch (ParseException e)
        {
            Assert.fail();
            return;
        }
        PeselExtractor extractor = new PeselExtractor( pesel);
        String sex = extractor.GetSex();
        Assert.assertEquals(shouldBeSex, sex);
    }
}
