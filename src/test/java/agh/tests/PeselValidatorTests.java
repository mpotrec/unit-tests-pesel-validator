package agh.tests;

import agh.qa.PeselValidator;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                {"44051401359", true},
                {"99920135363", true}, //1899
                {"00123122298", true}, //1900-12-31
                {"00323187891", true}, //2000-12-31
                {"00523155957", true}, //2100-12-31
                {"00723155953", true}, //2200-12-31
                {"00500778614", true}, //2100
                {"99661245671", false}, //2200
                {"20222954778", true}, // 29Feb Leap
                {"19222854843", false}, // 29Feb !Leap
                {"20223054778", false}, // 30Feb Leap
                {"8502096912", false}, // too short
                {"610405933143", false}, // too long
                {"701228w9444", false}, // invalid character
                {"61043293314", false}, // invalid day
                {"00342598749", false}, // invalid month
                {"18242598749", false}, // invalid year
                {"73093115993", false}, // invalid day in month
                {"86061539617", false} // wrong checkSum ?
    };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid){
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}
